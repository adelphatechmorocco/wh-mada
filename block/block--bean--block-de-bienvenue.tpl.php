<?php 
$el = $elements['bean']['block-de-bienvenue']['#entity'];
?>
<div class="bienvenu welcome bienvenu-france <?php print $classes; ?>" <?php print $attributes; ?>>
    <?php
    print render($title_suffix);
    ?>
    <div class="bienvenu-text wow zoomIn text-center"  data-wow-duration="2s">
        <?php print $el->field_description_courte[LANGUAGE_NONE]['0']['value']; ?>
    </div>
</div>