<?php 
$el = $elements['bean']['et-toi-que-cherches-tu--0']['#entity'];
$el2 = $elements['bean']['et-toi-que-cherches-tu--0']['field_boutons'];
$n = count($el2['#items']);
?> 
<div id="que-cherches-tu" class="que-cherches-tu attentes-wh <?php print $classes; ?>" <?php print $attributes; ?>>
    <?php
    print render($title_suffix);
    ?>
    <div class="content-que-cherches-tu">
        <h2 class="text-center wow zoomIn"><?php print $el->title; ?></h2>
        <?php 
       for($i=0; $i<$n; $i++):
       $m=$i+1;
       $index = $el2['#items'][$i]['value'];
       $pon=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_on']['#items'][0]['uri']); 
       $poff=file_create_url($el2[$i]['entity']['field_collection_item'][$index]['field_picto_off']['#items'][0]['uri']); 
       if($i%2==0){
           $c="fadeInUp";
       }else{
           $c="fadeInDown";
       }
       ?>
            <div class="critere-item wow <?=$c?>">
                <a href="<?=url($el2[$i]['entity']['field_collection_item'][$index]['field_lien']['#items'][0]['value'])?>">
                    <div class="critere-img ">
                        <div class="imgs-cherche cf">
                            <img alt="" class="top" src="<?=$poff?>">
                            <img alt="" class="bottom" src="<?=$pon?>">
                        </div>
                    </div>
                    <div class="critere-text">
                        <span><?=$el2[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></span>
                    </div>
                </a>
            </div>
        <?php endfor; ?>
    </div>
</div>