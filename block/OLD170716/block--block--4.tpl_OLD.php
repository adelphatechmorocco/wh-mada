<?php
/**
 * @file
 * Zen theme's implementation to display a block.
 *
 * Available variables:
 * - $title: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In
 *     that case the class would be "block-user".
 *   - first: The first block in the region.
 *   - last: The last block in the region.
 *   - odd: An odd-numbered block in the region's list of blocks.
 *   - even: An even-numbered block in the region's list of blocks.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see zen_preprocess_block()
 * @see template_process()
 * @see zen_process_block()
 */
?>

<!-- Bloc Nous rejoindre -->
<div id="bloc_rejoingner" class="bloc_rejoindre">
    <div class="arr_right_top"></div>
    <?php if ($logged_in == FALSE): ?>
        <div class="inscription">
    	<h3>Nous rejoindre</h3>
    	<div class="visuel"><img src="<?php print drupal_get_path("theme", "webhelp"); ?>/images/visuels/vis_rejoindre.png" alt="" title="" /></div>
    	<div class="form_rejoindre">
    	    <div class="links">
    		<a href="#bloc_rejoingner" class="connex" title="<?php print t('Déjà inscrit'); ?>"><?php print t("Déjà inscrit"); ?></a>
    		<a href="#bloc_rejoingner" class="connex connexion" title="<?php print t('Connexion'); ?>"><?php print t("Connexion"); ?></a>    
    	    </div>
    	    <form name="f_rejoindre" id="form_inscrire" method="post" action="user/register">
    		<p><input type="text" name="nom" class="input_text" onfocus="if (this.value == 'Nom') {this.value = ''}" onblur="if (this.value == '') {this.value = 'Nom'}" value="Nom"/></p>
    		<p><input type="text" name="prenom" class="input_text" onfocus="if (this.value == 'Prénom') {this.value = ''}" onblur="if (this.value == '') {this.value = 'Prenom'}" value="Prénom "/></p>
    		<p><input type="text" name="mail" class="input_text" onfocus="if (this.value == 'Email') {this.value = ''}" onblur="if (this.value == '') {this.value = 'Email'}" value="Email"/></p>
    		<p><input type="image" src="<?php print drupal_get_path("theme", "webhelp"); ?>/images/pictos/bt_valider.png" alt="valider" class="input_img"/></p>
    	    </form>
    	</div>
        </div>

        <div class="authentifier">
    	<h3>Nous rejoindre</h3>
    	<div class="visuel"><img src="<?php print drupal_get_path("theme", "webhelp"); ?>/images/visuels/vis_rejoindre.png" alt="" title="" /></div>
    	<div class="form_rejoindre">
    	    <div class="links">
    		<a href="#bloc_rejoingner" class="Inscription connexion" title="<?php print t('Inscription '); ?>"><?php print t("S'inscrire"); ?></a>

    	    </div>
		<?php
		$form = drupal_get_form("user_login");
		print render($form);
		?>
    	</div>
        </div>
    <?php else : ?>
        <h3>Nous rejoindre</h3>
        <div class="visuel"><img src="<?php print drupal_get_path("theme", "webhelp"); ?>/images/visuels/vis_rejoindre.png" alt="" title="" /></div>

	<?php
	global $user;
	$utilisateur = user_load($user->uid);
	$nom = $utilisateur->field_nom["und"][0]["value"];
	print "<p class='bienvenue'>Bienvenue " . $utilisateur->field_prenom["und"][0]["value"] . "</p>";
	?>
        <p class='monespace'> 
	    <?php print l("Mon CV", "cv-webhelp"); ?>  </p>
        <p class='monespace2'>  
	    <?php print l("Informations personnelles", "user/" . $user->uid . "/edit"); ?> </p>
        <p class='espace-deconnexion'> <?php print l("Déconnexion", "user/logout"); ?> </p>
        <span class="show_form-1"></span>


    <?php endif ?>
    <div class="arr_right_bas"></div>
</div>