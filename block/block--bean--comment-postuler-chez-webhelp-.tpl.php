<?php 
/*$el = $elements['bean']['comment-postuler-chez-webhelp-']['#entity'];
$el1 = $elements['bean']['comment-postuler-chez-webhelp-']['field_les_plus_1'];
$n = count($el1['#items']);
$el2 = $elements['bean']['comment-postuler-chez-webhelp-']['field_les_plus_2'];
$o = count($el2['#items']);
$el3 = $elements['bean']['comment-postuler-chez-webhelp-']['field_etapes2'];
$p = count($el3['#items']);
?>  
 <div class="postuler-webhelp <?php print $classes; ?>" <?php print $attributes; ?> id="postuler-webhelp">
	 <?php
    print render($title_suffix);
    ?>
    <div class="container">
        <div class="content-postuler-webhelp">
            <h2 class="text-center wow zoomIn"><?=$el->title?></h2>
            <p class="text-center choix-postuler"><?php print $el->field_sous_titre[LANGUAGE_NONE]['0']['value']?></p>
            <div class="methodes-postuler">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="chrono wow zoomIn">
                        <p><?php print $el->field_nom[LANGUAGE_NONE]['0']['value']?></p>
                        <div class="explication-chrono explication-methode">
                            <?php print $el->field_description_courte[LANGUAGE_NONE]['0']['value']?>
                        </div>
                        <div class="avantages-chrono avantages">
                            <span>Les</span>
                            <?php for($i=0; $i<$n; $i++):
				            $index = $el1['#items'][$i]['value'];
				            ?>
                            <p><span><?=$el1[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?> : </span><?=$el1[$i]['entity']['field_collection_item'][$index]['field_description']['#items'][0]['value']?></p>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="online wow zoomIn"  data-wow-duration="2s">
                        <p><?php print $el->field_titre_2[LANGUAGE_NONE]['0']['value']?></p>
                        <div class="explication-online explication-methode">
                            <?php print $el->field_description[LANGUAGE_NONE]['0']['value']?>
                            <p class="astuce"><span>Astuce: </span><?php print $el->field_astuces[LANGUAGE_NONE]['0']['value']?></p>
                        </div>
                        <div class="avantages-online avantages">
                            <span>Les</span>
                            <?php for($i=0; $i<$o; $i++):
				            $index = $el2['#items'][$i]['value'];
				            ?>
                            <p><span><?=$el2[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?> : </span><?=$el2[$i]['entity']['field_collection_item'][$index]['field_description']['#items'][0]['value']?></p>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="suite-postuler">
                <h3 class="text-center"><?php print $el->field_titre_suite[LANGUAGE_NONE]['0']['value']?></h3>
                <div class="etapes">
	                <?php for($i=0; $i<$p; $i++):
		            $index = $el3['#items'][$i]['value'];
		            $pon=file_create_url($el3[$i]['entity']['field_collection_item'][$index]['field_picto_on']['#items'][0]['uri']); 
		            ?>
	                    <div class="etape1 item-etape wow zoomIn">
	                        <img alt="" src="<?=$pon?>" class="center-block">
	                        <div class="text-etape text-center"><?=$el3[$i]['entity']['field_collection_item'][$index]['field_nom']['#items'][0]['value']?></div>
	                    </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
        <div class="img-wh">
            <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-wh.png">
        </div>
    </div>
</div>

*/
?>
<div class="etapes-recrutement postuler-webhelp" id="postuler-webhelp">
    <div class="container">
        <h2 class="text-center">Les étapes du recrutement</h2>
        <ul class="nav nav-tabs" role="tablist">
            <li class="active tab1">
                <a href="#postuler" role="tab" data-toggle="tab">
                    <div class="etape1 item-etape wow fadeInUp">
                        <div class="item-etape-content">
                            <div class="img-etape">
                                <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-candidature.png" class="center-block top">
                                <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-candidature-active.png" class="center-block bottom">
                            </div>
                            <p>Comment <br/>postuler ?</p>
                            <div class="number-etape">
                                1
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <li class="tab2">
                <a href="<#etude" role="tab"  data-toggle="tab">
                    <div class="etape2 wow zoomIn">
                        <div class="item-etape-content">
                            <div class="img-etape">
                                <img alt="" class="top center-block" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-phone.png">
                                <img alt="" class="bottom center-block" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-phone-active.png">
                            </div>

                            <div class="imgs-cherche imgs-box cf">

                            </div>
                            <p>L'étude de votre <br/>candidature</p>
                            <div class="number-etape">
                                2
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <li class="tab3">
                <a href="#test" role="tab" data-toggle="tab">
                    <div class="etape3 wow fadeInUp">
                        <div class="item-etape-content">
                            <div class="img-etape">
                                <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-test.png" class="center-block top">
                                <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-test-active.png" class="center-block bottom">
                            </div>
                            <p>Le déroulé <br/>des entretiens <br/>et des tests</p>
                            <div class="number-etape">
                                3
                            </div>
                        </div>
                    </div>

                </a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane fade active in" id="postuler">
                <div class="comment-postuler">
                    <h5>Candidature en ligne</h5>
                    <div class="item-tab">
                        <h6>Postuler à une offre en quelques clics</h6>
                        <p>Vous sélectionnez l'offre qui vous intéresse, vous créez votre compte.<br/>
                            Vous renseignez votre dossier de candidature en y joignant votre CV.</p>
                        <h6>Déposer sa candidature spontanée</h6>
                        <p>Vous créez votre compte candidat puis renseignez votre dossier de candidature <br/>en y joignant votre CV.</p>
                        <div class="avantages-online avantages">
                            <span>Les  + :</span> Rapidité, Simplicité, Efficacité
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="etude">
                <div class="comment-postuler">
                    <h5>Tous les dossiers de candidatures sont étudiés par nos équipes de recrutement</h5>
                    <div class="item-tab">
                        <h6>Candidature</h6>
                        <p>Une première sélection est effectuée sur CV. </p>
                        <h6>Téléphone</h6>
                        <p>Le candidat est contacté pour sa pré-sélection.</p>
                    </div>

                </div>

            </div>
            <div class="tab-pane fade" id="test">
                <div class="comment-postuler">
                    <div class="item-tab">
                        </p>
                        <h6>Entretien individuel </h6>
                        <p>Chaque candidat est reçu par un chargé de recrutement afin de valider ses <br/>compétences, sa motivation et l'adéquation de son profil avec nos <br/>opportunités.  </p>
                        <h6>Tests, quizz et mises en situation </h6>
                        <p>En fonction du recrutement, le candidat présélectionné sera amené à réaliser <br/>divers tests, quizz et mises en situation.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="img-wh">
            <img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-wh.png">
        </div>
    </div>

</div>