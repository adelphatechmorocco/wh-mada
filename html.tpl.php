<?php
global $user;
?>
<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="https://www.facebook.com/2008/fbml"
      xmlns:og="http://opengraphprotocol.org/schema/">
<!--<![endif]-->
<!-- Smartsupp Live Chat script -->
<?php if(!empty($_GET['dev'])){ ?>
<script type="text/javascript">
var _smartsupp = _smartsupp || {};
_smartsupp.key = 'd1a2a51a6d886db9c83ed62a061b26fa6933e267';
window.smartsupp||(function(d) {
    var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
    s=d.getElementsByTagName('script')[0];c=d.createElement('script');
    c.type='text/javascript';c.charset='utf-8';c.async=true;
    c.src='//www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);
</script>
<?php } ?>

<head>
    <meta charset="utf-8">

	<meta http-equiv="Expires" content="Tue, 01 Jan 1995 12:12:12 GMT">
	<meta http-equiv="Pragma" content="no-cache">

    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>


    <title><?php print $head_title; ?></title>



<?php  print $head; ?>
<?php // print $styles; ?>

<?php
if($_SERVER['REQUEST_URI']=='/mada/rse')
{
?>

<link type="text/css" rel="stylesheet" href="http://5.79.1.197/sites/default/files/css/css_kShW4RPmRstZ3SpIC-ZvVGNFVAi0WEMuCnI0ZkYIaFw.css?v=2" media="all" />
<link type="text/css" rel="stylesheet" href="http://5.79.1.197/sites/default/files/css/css_-TNq6F6EH1K3WcBMUMQP90OkyCq0Lyv1YnyoEj3kxiU.css?v=2" media="screen" />
<link type="text/css" rel="stylesheet" href="http://5.79.1.197/sites/default/files/css/css_eP3VB0Z_bRgYxTSSIr99LLt4OS3PK8pY0LgxwlzM588.css?v=2" media="all" />
<link type="text/css" rel="stylesheet" href="http://5.79.1.197/sites/default/files/css/css_q_7BmOQoJ31zYDaQfUfeyd0HslhC8Rmp0dfq8irAl_Y.css?v=2" media="all" />
<link type="text/css" rel="stylesheet" href="http://5.79.1.197/sites/default/files/css/css_GOmkOs_F5v9EGcE4ZvqOlpwlj1IYvcmnLgoMs08nzus.css?v=2" media="all" />

 <style type="text/css">
.conseil-wh-france {
    padding-top: 107px !important;
    padding-bottom: 55px !important;
}
.conseil-wh-france h2 {
       font-size: 38px;
    padding-bottom: 0;
}
</style>
<?php
}
else
{

	print $styles;

}
 ?>


<?php print $scripts; ?>
  <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/mycustom.css">


    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
    <link rel="shortcut icon" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/nanoscroller.css">
	<!--[if IE 8]>
	    <link rel="stylesheet" type="text/css" href="<?php print $base_path; ?>sites/all/themes/webhelp/styles/style_ie8.css" media="all" />
	<![endif]-->
	<!--[if IE 7]>
	   <link rel="stylesheet" type="text/css" href="<?php print $base_path; ?>sites/all/themes/webhelp/styles/style_ie7.css" media="all" />
	<![endif]-->

	<!--[if IE 6]>
	<script src="js/png.js" language="javascript"></script>
	<script language="javascript">
	DD_belatedPNG.fix('.png');
	</script>
	    <![endif]-->

</head>
<body>

	<div id="wrapper">
		<?php print $page_top; ?>
		<?php print $page; ?>
		<?php print $page_bottom; ?>
        <?php include './'. path_to_theme() .'/templates/page/footer.tpl.php'; ?>
    </div>
	<!-- Includes all scripts -->

	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/menu.js"></script>
	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/wow.min.js"></script>
	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/mouse-check-element.min.js"></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
	<script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.counterup.js"></script>
	<script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/videoLightning.js"></script>
	<script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap-carousel.js"></script>
	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.bxslider.js"></script>
	<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.themepunch.plugins.min.js"></script>
	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.themepunch.revolution.min.js"></script>
	<script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/Placeholders.js"></script>
    <script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/select2.full.min.js"></script>
	<script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.nanoscroller.min.js"></script>
    <script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.maskedinput.min.js"></script>
	<script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.flip.min.js"></script>

	<script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js?v=1"></script>


</body>

