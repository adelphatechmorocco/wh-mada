<?php
/**
 * @file
 * Zen theme's implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   - view-mode-[mode]: The view mode, e.g. 'full', 'teaser'...
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 *   The following applies only to viewers who are registered users:
 *   - node-by-viewer: Node is authored by the user currently viewing the page.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $pubdate: Formatted date and time for when the node was published wrapped
 *   in a HTML5 time element.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content. Currently broken; see http://drupal.org/node/823380
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess_node()
 * @see template_process()
 */
 global $user;
global $base_url;
?>

<?php

cvsponsor_rebuild_url($node->nid);
trackingVisitsCount($node->nid, $_GET['s']);
trackingOffreVisits($node->nid, $_GET['s'],$_GET['w']);

if($user->uid && in_array('administrator', $user->roles))
	    	print "<a href='".$base_url."/node/".$node->nid."/edit?destination=node/".$node->nid."' style='float:right; clear:both'>Editer </a>";


if ($teaser):
?>
    <div class="views-row-<?php echo $zebra; ?>" >

        <h2><?php print l($node->title, 'node/' . $node->nid) ?></h2>
        <p class="desc_offre">
	    <?php
print $node->field_description_courte_offre['und'][0]['value'];
?>
        </p>
        <p class="lire_plus"><?php print l(t('Lire plus'), 'node/' . $node->nid) ?></p>
    </div>
<?php else: ?>
    <?php print '<h2 class="supertitre">' . t('Offre d\'emploi') . '</h2>'; ?>
    <div class="bloc_partage">
        <p>
	    <?php print t('Partager sur'); ?>
        </p>
	<?php print render($content['links']);
?>
    </div>
    <div class="bloc_text">
	<?php print "<h2>" . date('d-m-Y', $node->changed) . ' : ' . $title . "</h2>"; ?>

	<?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || !$page && $title): ?>

	    <?php print render($title_prefix); ?>
	    <?php if (!$page && $title): ?>
	        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
	    <?php endif; ?>
	    <?php print render($title_suffix); ?>

	    <?php if ($display_submitted): ?>
	        <p class="submitted">
		    <?php print $user_picture; ?>
		    <?php print $submitted; ?>
	        </p>
	    <?php endif; ?>

	    <?php if ($unpublished): ?>
	        <p class="unpublished"><?php print t('Unpublished'); ?></p>
	    <?php endif; ?>

	<?php endif; ?>

	<?php
// We hide the comments and links now so that we can render them later.
//hide($content['comments']);
//hide($content['links']);
print render($content);
?>

	<?php //print render($content['links']);   ?>

	<?php print render($content['comments']); ?>
	<?php if ( $user->uid ): ?>
	    <?php
	
$nid = getNodeByUidByType($user->uid, "cv_webhelp");
?>
	    <?php if ($nid): ?>
				<?php if(!empty($_GET['ref'])){
				switch ($_GET['ref']) {
					case 'www.viadeo.com':
						$ref =  "viad";
						break;
					case 'www.facebook.com':
						$ref = "fc";
					case 'l.facebook.com':
						$ref = "fc";
						break;
					case 'www.youtube.com':
						$ref = "ytb";
						break;
					case 'www.linkedin.com':
						$ref = "in";
						break;
					case 'google.com':
						$ref = "gle";
						break;
					}
				}
				
				if(!empty($_GET['w'])){  $w = '/'.$_GET['w'];}
				if(!empty($ref)){ $ref  = '/'. $ref; }
				
				?>
				<div class="postuler"><?php echo l('<img src="sites/all/themes/webhelp/images/pictos/bt_postuler.png" alt=" Postuler à cette offre"  />', 'recrute/postuler/' . arg(1) . $ref . $w, array('html' => true)) ?></div>
	    <?php else : ?>
	        <div class="postuler"><?php echo l(t('Créez un CV pour postuler à cette offre'), 'cv-webhelp') ?></div>
	    <?php endif; ?>

	<?php else : ?>
	    <div class="postuler"><?php echo l('<img src="sites/all/themes/webhelp/images/pictos/bt_sauthentifier.png" alt="S authentifier pour postuler à cette offre" />', 'user/login', array('html' => true, 'query' => drupal_get_destination())) ?></div>

	<?php endif; ?>
        <div class="retour"><?php echo l(t('Retour à la liste'), 'offre-emploi') ?></div>

    </div>

<?php endif; ?>


<?php kpr($node); ?>

<div class="details-offre">
    <div class="content-details-offres">
        <h2>Chef de Projet Télévente Sénior (H/F)</h2>
        <div class="top-details-offres">
            <p><span>Ville :</span> Agadir</p>
            <p><span>Métier :</span> Chef de projet</p>
            <p><span>Contrat :</span> CDI</p>
			<?php if ($logged_in): ?>
	    <?php
	    global $user;
	    $nid = getNodeByUidByType($user->uid, "cv_webhelp");
	    ?>
	    <?php if ($nid): ?>
			<?php if(!empty($_GET['ref'])){
			switch ($_GET['ref']) {
						case 'www.viadeo.com':
							$ref =  "viad";
							break;
						case 'www.facebook.com':
							$ref = "fc";
						case 'l.facebook.com':
							$ref = "fc";
							break;
						case 'www.youtube.com':
							$ref = "ytb";
							break;
						case 'www.linkedin.com':
							$ref = "in";
							break;	
						case 'google.com':
							$ref = "gle";
							break;		
			}
			}
			
			if(!empty($_GET['w']))  $w = '/'.$_GET['w'];
	        if(!empty($ref)) $ref  = '/'. $ref;
			
			?>
		        
		    	<div class="link-postuler">
	                <a href="<?php print '/recrute/postuler/' . arg(1) . $ref . $w; ?>">Postuler</a>
	            </div>
		<?php else : ?>
			<div class="link-postuler">
                <a href="<?php print '/cv-webhelp'; ?>">Postuler</a>
            </div>
	    <?php endif; ?>

	<?php else : ?>
		<div class="link-postuler">
                <a href="<?php print '/user/login'; ?>">Postuler</a>
            </div>
	<?php endif; ?>
        
        </div>
        <p class="text-mission">Rattaché au Directeur de production, vous aurez comme missions de :</p>
        <div class="mission-offres mission-offres1">
            <h5>Gestion de la production</h5>
            <p>Prendre en charge le pilotage opérationnel : </p>
            <p>- Concilier les objectifs de la production, la qualité et les exigences client. </p>
            <p>- Planifier, optimiser les effectifs à moyen terme et valider les recrutements.</p>
            <p>- Assurer la disponibilité des moyens techniques de production (matériel et outils)</p>
            <p>- Respecter les processus (P02, P05, P07) et normes qualité (ISO, NF345) sur votre projet et     respect des méthodes qualité.</p>
            <p>- Gestion des incidents.</p>
            <p>Veiller à l'atteinte des objectifs quantitatifs et qualitatifs.</p>
            <p>Analyser, proposer et mettre en œuvre des plans d’action nécessaires pour atteindre les objectifs sur votre projet.</p>
        </div>
        <div class="mission-offres mission-offres2">
            <h5>Gestion de la relation avec le client donneur d’ordre</h5>
            <p>Comprendre et adhérer aux enjeux des clients donneurs d’ordre.</p>
            <p>Elaborer les reporting du projet (quotidiens, hebdomadaires, mensuels, comités de pilotage, comités de production).</p>
            <p>Contribuer à l’apport de valeur ajoutée sur son projet (CFE, LEAN, optimisation du parcours consommateur, …).</p>
            <p>Aviser en cas de nécessité en respectant les procédures d’alerte.</p>
            <p>Gérer les réclamations clients en l'absence du Directeur de Comptes ou par délégation</p>
        </div>
        <div class="link-voir-plus">
            <a href="#">Voir plus</a>
        </div>
    </div>
</div>


 