<?php
/**
 * @file jcarousel-view.tpl.php
 * View template to display a list as a carousel.
 */
?>

<?php foreach ($rows as $id => $row): ?>

<div class="listVideotheque " >  
    <h3><?php print $title; ?></h3>
    <?php echo'<ul id="foo'.$id.'" class=" mycarousel jcarousel-skin-default">'; ?> 
    <?php print $row; ?>
    <?php echo " </ul>" ?>
</div>
    <?php jcarousel_add('mycarousel', array('horizontal' => TRUE)); ?>
<?php endforeach; ?>
