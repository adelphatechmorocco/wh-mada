<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php foreach ($rows as $id => $row): ?>
  <div class="wrapper-box-gray">
    <?php print $row; ?>
  </div>
<?php endforeach; ?> 
            