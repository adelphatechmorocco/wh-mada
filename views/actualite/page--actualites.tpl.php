<?php
$pclass = "top-slide-actualites";
$ptitre = "Actualités";
include './' . path_to_theme() . '/templates/page/header.tpl.php';
?>

<div id="content">
    <?php print $messages; ?>
    <div class="nos-offres-actualites">
        <div class="content-offres-actualites">
            <!--div class="top-bloc-newsletter">
                <div class="content-newsletter">
                    <p class="text-center">Pour suivre nos actualités, retrouvez ici votre rendez-vous mensuel avec la news
                        Webhelp Carrières :<br /><br /><br /></p>
                    <div class="orange-link">
                        <a href="<?= base_path(); ?><?php print drupal_get_path_alias('node/52875'); ?>"
                           class="newsletter-link">Newsletter</a>
                    </div>
                </div>
            </div-->
            <div class="offres-actualites actualites">
                <?php
                print render($page['content']);
                ?>
            </div>
        </div>
    </div>
    <?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>



