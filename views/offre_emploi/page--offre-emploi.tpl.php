<?php
$pclass = "top-slide-offres";
$ptitre = "Nos offres";
$stitre = "Trouvez le job qui est fait pour vous !";
include './'. path_to_theme() .'/templates/page/header.tpl.php';
?>
<div id="content">

<?php print $messages; ?>
<div class="nos-offres-actualites">
    <div class="content-offres-actualites">
        <div class="offres-actualites offres">
	         <?php $view = views_get_view("offre_emploi");
		         $view->set_display('page');
		         $view->pre_execute();
				 $view->execute();
?>
<div class="trouver-emploi">
        <h3 class="text-center title-offre">Trouver un emploi</h3>
        <h3 class="text-center title-not-found hidden">Nous n’avons malheureusement aucune offre à pourvoir selon les critères saisis : n’hésitez pas à élargir votre recherche !</h3>
        <div class="input-group my-group">
            <?php
            $display_id = 'page';
            $view->set_display($display_id);
            $view->init_handlers();
            $form_state = array(
            'view' => $view,
            'display' => $view->display_handler->display,
            'exposed_form_plugin' => $view->display_handler->get_plugin('exposed_form'),
            'method' => 'get',
            'rerender' => TRUE,
            'no_redirect' => TRUE,
            );
            $form = drupal_build_form('views_exposed_form', $form_state);
            print drupal_render($form);
            ?>
        </div>
    <p class="text text-center"><span><?=$view->total_rows?> </span>offre(s) en ligne </p>
</div>
            <?php
            print render($page['content']);
            ?>
        </div>
    </div>
</div>

<?php include './'. path_to_theme() .'/templates/page/block-newsletter.tpl.php'; ?>
