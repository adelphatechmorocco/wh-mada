<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div class="carousel slide vertical" data-ride="carousel" id="quote-carousel2">
    <div class="webhelp-offres">
        <div class="carousel-inner">
            <?php foreach ($rows as $id => $row): ?>
            	<?php print $row; ?>
            <?php endforeach; ?> 
        </div>
    </div>
    <div class="carousel-jobs">
        <a data-slide="prev" href="#quote-carousel2" class="prev right carousel-control"></a>
        <a data-slide="next" href="#quote-carousel2" class="next-job right carousel-control"></a>
    </div>
</div>