<footer id="footer" class="footer-france">
    <div class="footer-top" id="footertop">
        <div class="content-footer footer-pages">
            <div class="logo-footer wow zoomIn animated" style="visibility: visible; animation-name: zoomIn;">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/logo-footer.png" alt="" class="logo-foot center-block">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/images/map-footer.png" alt="" class="img-responsive map-footer-top">
            </div>
            <div class="menu-footer wow zoomIn animated" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: zoomIn;">
                <ul>
                    <li class="menu-items menu-items-wh">
                        <a>Qui sommes-nous ?</a>
                        <ul>
                            <li><a href="<?php print url('webhelp-madagascar'); ?>">Webhelp Madagascar</a></li>
                            <li><a href="http://webhelp.com/">Webhelp Monde</a></li>
                            <li><a href="<?php print url('<front>'); ?>#que-cherches-tu">Qu’attendez-vous</a></li>
                            <li><a href="<?php print url('actualites'); ?>">Actualités</a></li>
                        </ul>
                    </li>
                    <li class="menu-items menu-items-avenir">
                        <a>Votre avenir avec nous</a>
                        <ul>
                            <li><a href="<?php print url('webhelp-university-0'); ?>">Formation</a></li>
                            <li><a href="<?php print url('nos-metiers-0'); ?>">Nos Métiers</a></li>
                            <li><a href="<?php print url('talent-management'); ?>">Évolution</a></li>
                        </ul>
                    </li>
                    <li class="menu-items menu-items-famille">
                        <a>Votre nouvelle famille</a>
                        <ul>
                            <li><a href="<?php print url('bien-etre'); ?>">Bien-être</a></li>
                            <li><a href="<?php print url('rse'); ?>">RSE </a></li>
                            <li><a href="<?php print url('avantages'); ?>">Avantages </a></li>
                            
                            <li><a href="<?php print url('fun-0'); ?>">Fun</a></li>
                        </ul>
                    </li>
                    <li class="menu-items menu-items-aventure">
                        <a>Envie de nous rejoindre</a>
                        <ul>
                            <li><a href="<?php print url('<front>'); ?>#postuler-webhelp">Recrutement</a></li>
                            <li><a href="<?php print url('<front>'); ?>#integration">Intégration</a></li>
                            <li><a href="<?php print url('infographies-rh1'); ?>">Nos Conseils RH</a></li>
                            <li><a href="<?php print url('offre-emploi'); ?>">Nos offres</a></li>
                            <li><a href="<?php print url('user/login'); ?>">Rejoignez-nous</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="suivez-nous">
                <div class="content-suivez-nous">
                    <span>Suivez-nous :</span>
                    <ul>
                        <li><a target="_blank" href="https://www.linkedin.com/company/webhelp"><i class="fa fa-linkedin"></i></a></li>
                        <li><a target="_blank" href="https://www.facebook.com/webhelpmadagascar"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="https://www.youtube.com/user/WebhelpTV"><i class="fa fa-youtube-play"></i></a></li>
                      <!--   <li><span class="myshare"><i class="fa fa-instagram"></i></span></li> -->
<!--                         <a href="#" 
  onclick="
  window.open(
    'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(window.location.href), 
    'facebook-share-dialog', 
    'width=626,height=436'); 
    return false;">
  Share on Facebook
</a> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom footer-bottom-pages">
        <div class="content-footer-bottom">
            <p class="text-center">Conformément à la loi "Informatique et Libertés" du 6 janvier 1978 modifiée en 2004, vous disposez d'un droit d'accès, de rectification et d'opposition aux informations personnelles vous concernant, que vous pouvez exercer auprès de Webhelp à l'adresse : "Service sécurité, 161 rue de Courcelles 75017 Paris" ou par e-mail à l'adresse : "contact_cnil@fr.webhelp.com " en joignant un justificatif d'identité </p>
            <p class="copyright text-center">© 2017 Webhelp, All Rights Reserved. <a href="<?=$GLOBALS['base_url']?>/conditions-dutilisation">Conditions d'utilisations </a></p>
        </div>
    </div>

</footer>
<div id="oldnavigator">
    <h2>Votre navigateur est obsolète.</h2>
    <p>Attention, sans la mise à jour de votre navigateur, <br>
        la visualisation des pages du site Webhelp France ne sera pas <br>
        optimale ! Nous vous invitons à mettre à jour votre navigateur :</p>
    <div class="buttons">
        <a href="#"><img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nav-chrome.png"></a>
        <a href="#"><img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nav-ie.png"></a>
        <a href="#"><img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nav-safari.png"></a>
        <a href="#"><img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nav-firefox.png"></a>
        <a href="#"><img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nav-opera.png"></a>
    </div>
</div>
<ul class="share-btn hidden" id="dvid">
    <li>
        <a href="<?=base_path();?>node/118">
            <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mail.png" alt="">
        </a>
        <span>Candidature express</span>
    </li>
    <!-- <li>
            <a href="#chat">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/bubble.png" alt="">
            </a>
            <span>Pose tes questions, nous répondons</span>
        </li>
        <li>
            <a href="#call">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/call.png" alt="">
            </a>
            <span class="large">Tu veux passer ton entretien ?  <br>
                Nous te rappelons tout de suite,  <br>
                ou à <a href="#">l’heure qui t’arrange</a>
            </span>
        </li> -->
    <li class="list-share">
        <a href="#share">
            <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/share.png" alt="">
        </a>
        <ul>
            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#" class="gplus"><i class="fa fa-google-plus"></i></a></li>
        </ul>
    </li>
    <li class="goto">
        <a class="scroll-top" href="#">
            <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/images/prev.png" alt="">
        </a>
    </li>
</ul>

<div class="popin" style="overflow-y: scroll">
    <a href="#" class="close-popin">×</a>
    <ul class="share-btn">
        <li>
            <a href="<?=base_path();?>node/52863">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mail.png" alt="">
            </a>
        </li>
        <!--li>
                <a href="#chat">
                    <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/bubble.png" alt="">
                </a>
            </li-->
        <li>
            <a href="#call">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/call.png" alt="">
            </a>
        </li>
    </ul>
    <div class="call">

        <div class="call-phone">
            <h2>Vous voulez passer votre <br> entretien ?</h2>
            <p style="margin-bottom: 50px;">Nous vous rappelons tout de suite : </p>
            <p class="cb-success" style="margin: -30px 0 20px 0; display: none;">Chargement...</p>
            <?php if ( $user->uid ) {
                $account = user_load($user->uid);
                ?>
                <form action="" method="post" name="callback-form" id="callback-form1">
                    <div class="formline first">
                        <input type="text" value="<?=$account->field_nom["und"][0]["value"]?>" id="cnom" name="nom" disabled placeholder="Nom">
                    </div>
                    <div class="formline first">
                        <input type="text" value="<?=$account->field_prenom["und"][0]["value"]?>" id="cprenom" name="prenom" disabled placeholder="Prénom">
                    </div>
                    <div class="formline first">
                        <input type="text" value="<?=$user->mail?>" id="cemail" name="email" disabled placeholder="Email">
                    </div>
                    <div class="formline first">
                        <input type="text" value="<?=$account->field_tel["und"][0]["value"]?>" id="cphone" name="cphone" placeholder="Téléphone">
                    </div>
                    <div class="formline first">
                        <select name="cregion" style="width: 100%;">
                            <option value="">Choisis une région</option>
                            <option value="Agadir" <?php if($account->field_region["und"][0]["value"]=="Agadir") { print 'selected'; } ?>>Agadir</option>
                            <option value="Fes" <?php if($account->field_region["und"][0]["value"]=="Fes") { print 'selected'; } ?>>Fès</option>
                            <option value="Kenitra" <?php if($account->field_region["und"][0]["value"]=="Kenitra") { print 'selected'; } ?>>Kénitra</option>
                            <option value="Meknes" <?php if($account->field_region["und"][0]["value"]=="Meknes") { print 'selected'; } ?>>Meknès</option>
                            <option value="RabatSale" <?php if($account->field_region["und"][0]["value"]=="RabatSale") { print 'selected'; } ?>>Rabat-Salé</option>
                        </select>
                    </div>

                    <input type="hidden" value="<?=$account->uid?>" id="cuid" name="cuid" placeholder="">
                    <div class="formline">
                        <button type="submit" class="wb-btn">J’attends votre appel</button>
                    </div>
                </form>
            <?php }else{ ?>
                <form action="" method="post" name="callback-form" id="callback-form1">
                    <div class="formline first">
                        <input type="text" value="" id="cnom" name="cnom" placeholder="Nom">
                    </div>
                    <div class="formline first">
                        <input type="text" value="" id="cprenom" name="cprenom" placeholder="Prénom">
                    </div>
                    <div class="formline first">
                        <input type="text" value="" id="cemail" name="cemail" placeholder="Email">
                    </div>
                    <div class="formline first">
                        <input type="text" id="cphone" name="cphone" placeholder="Téléphone" value="">
                    </div>
                    <div class="formline first">
                        <input type="text" value="" id="cpassword" name="cpassword" placeholder="Mot de passe">
                    </div>
                    <div class="formline first">
                        <select name="cregion" style="width: 100%;">
                            <option value="">Choisis une région</option>
                            <option value="Agadir">Agadir</option>
                            <option value="Fes">Fès</option>
                            <option value="Kenitra">Kénitra</option>
                            <option value="Meknes">Meknès</option>
                            <option value="RabatSale">Rabat-Salé</option>
                        </select>
                    </div>
                    <div class="formline">
                        <button type="submit" class="wb-btn">J’attends votre appel</button>
                    </div>
                </form>
            <?php } ?>
            <!--a href="#" class="link">Choisir l’heure d’appel</a-->
            <p><br><br><strong>Horaires d’ouvertures hors jours fériés :</strong><br><br>

                Du Lundi au Vendredi<br>
                09h00 à 12h00<br>
                15h00 à 18h00<br></p>
        </div>

        <!--div class="call-hour">
            <h2>Choisis l’heure et la date qui te <br>conviennent :</h2>
            <form action="#" method="POST">
                <div class="formline time">
                    <select name="country" id="country" class="selectpicker">
                        <option value="23 sep">23 Sep</option>
                        <option value="24 sep">24 Sep</option>
                        <option value="25 sep">25 Sep</option>
                        <option value="26 sep">26 Sep</option>
                    </select>
                    <select name="country" id="country" class="selectpicker">
                        <option value="9">9:00</option>
                        <option value="10">10:00</option>
                        <option value="11">11:00</option>
                        <option value="12">12:00</option>
                    </select>
                </div>
                <div class="formline first">
                    <select name="country" id="country" class="popin-selectpicker">
                        <option value="+212">maroc</option>
                        <option value="+212">maroc</option>
                        <option value="+212">maroc</option>
                        <option value="+212">maroc</option>
                    </select>
                    <input type="text" value="+212" id="phone" name="phone">
                </div>
                <div class="formline">
                    <button type="submit" class="wb-btn">J’attends votre appel</button>
                </div>
            </form>
        </div-->

    </div>
    <div class="chat">

        <h2>Bienvenue</h2>

        <form action="#" method="POST">
            <div class="formline">
                <input type="text" id="name" name="name" placeholder="Nom">
            </div>
            <div class="formline">
                <input type="text" id="email" name="email" placeholder="Email">
            </div>
            <div class="formline submit">
                <button type="submit" class="wb-btn">Commencer le Tchat</button>
            </div>
        </form>
    </div>
    <div class="chatwindow">
        <h2>Pose tes questions, nous répondons</h2>
        <div class="box-chat">
            <div class="messages">
                <div class="push">
                    <span>Salam</span>
                </div>
                <div class="push left">
                    <span>Bonjour, Je suis ici, comment puis-je t'aider ?</span>
                </div>
            </div>

            <div class="zone">
                <textarea name="" id="" placeholder="Écris ton message ici"></textarea>
                <button><i class="fa fa-paper-plane"></i></button>
            </div>
        </div>
    </div>
</div> <!-- popin -->