<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */

global $user;
$term = taxonomy_term_load($node->field_categorie_actualite[LANGUAGE_NONE][0][tid]);
$name = $term->name;
?>
<header class="slide-pages">
    <div class="top-slide top-slide-offres">
        <?php
        $titre = '<div class="titre">
                <h1 class="text-center">Offre d\'emploi</h1>
                <h2 class="text-center">Trouvez le job qui est fait pour vous !</h2>
                </div>';
        include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
            </div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
               </header>

               <div id="content">
                
                <div class="content-page">
                    <div class=" top-page-content">
                       
                       <?php 
                       if($user->uid && in_array('administrator', $user->roles))
                        print "<a href='".base_path()."node/".$node->nid."/edit?destination=node/".$node->nid."' style='float:right; clear:both'>Editer</a>";
                    ?>
                    <div id="node-body">
                       <div class="details-offre">
                        <div class="content-details-offres">
                            <?php 
		                       print $messages; 
		                       ?>
                            
                            <h2><?=$node->title?></h2>
                            <div class="top-details-offres">
                                <?php 
                                $term = taxonomy_term_load($node->field_ville[LANGUAGE_NONE][0][tid]);
                                $ville = $term->name;   
                                $term2 = taxonomy_term_load($node->field_metier[LANGUAGE_NONE][0][tid]);
                                $metier = $term2->name;
                                if($node->field_contrat[LANGUAGE_NONE][0][value]==0)
                                    $contrat="ANAPEC";
                                elseif($node->field_contrat[LANGUAGE_NONE][0][value]==1)
                                    $contrat="CDD";
                                else
                                    $contrat="CDI";
                                ?>
                                <p><span>Ville :</span> <?=$ville?> </p>
                                <p><span>Métier :</span> <?=$metier?></p>
                                <!--p><span>Contrat :</span> <?=$contrat?></p-->
                                <?php
                                if ($logged_in):
                                    $nid = getNodeByUidByType($user->uid, "cv_webhelp");
                                ?>
                                <?php if ($nid): ?>
                                    <?php 
                                    if(!empty($_GET['ref'])){
                                        switch ($_GET['ref']) {
                                            case 'www.viadeo.com':
                                            $ref =  "viad";
                                            break;
                                            case 'www.facebook.com':
                                            $ref = "fc";
                                            case 'l.facebook.com':
                                            $ref = "fc";
                                            break;
                                            case 'www.youtube.com':
                                            $ref = "ytb";
                                            break;
                                            case 'www.linkedin.com':
                                            $ref = "in";
                                            break;  
                                            case 'google.com':
                                            $ref = "gle";
                                            break;      
                                        }
                                    }
                                    
                                    if(!empty($_GET['w']))  $w = base_path().$_GET['w'];
                                    if(!empty($ref)) $ref  = base_path().$ref;
                                    
                                    ?>
                                    
                                    <div class="link-postuler">
                                        <a href="<?php print base_path().'recrute/postuler/' . arg(1) . $ref . $w; ?>">Postuler</a>
                                    </div>
                                <?php else : ?>
                                    <div class="link-postuler">
                                        <a href="<?php print base_path().'cv-webhelp'; ?>">Postuler</a>
                                    </div>
                                <?php endif; ?>

                            <?php else : ?>
                                <div class="link-postuler">
                                    <a href="<?php print base_path().'user/login'; ?>">Postuler</a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php  print render($node->body[LANGUAGE_NONE][0][value]); ?>
                        <br> <br>
                        <div class="top-details-offres last">
                           <?php   if ($logged_in):
                           $nid = getNodeByUidByType($user->uid, "cv_webhelp");
                           ?>
                           <?php if ($nid): ?>
                            <?php 
                            if(!empty($_GET['ref'])){
                                switch ($_GET['ref']) {
                                    case 'www.viadeo.com':
                                    $ref =  "viad";
                                    break;
                                    case 'www.facebook.com':
                                    $ref = "fc";
                                    case 'l.facebook.com':
                                    $ref = "fc";
                                    break;
                                    case 'www.youtube.com':
                                    $ref = "ytb";
                                    break;
                                    case 'www.linkedin.com':
                                    $ref = "in";
                                    break;  
                                    case 'google.com':
                                    $ref = "gle";
                                    break;      
                                }
                            }
                            
                            if(!empty($_GET['w']))  $w = base_path().$_GET['w'];
                            if(!empty($ref)) $ref  = base_path(). $ref;
                            
                            ?>
                            
                            <div class="link-postuler">
                                <a href="<?php print base_path().'recrute/postuler/' . arg(1) . $ref . $w; ?>">Postuler</a>
                            </div>
                        <?php else : ?>
                            <div class="link-postuler">
                                <a href="<?php print base_path().'cv-webhelp'; ?>">Postuler</a>
                            </div>
                        <?php endif; ?>

                    <?php else : ?>
                        <div class="link-postuler">
                            <a href="<?php print base_path().'user/login'; ?>">Postuler</a>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="retour"><a href="<?=base_path();?>offre-emploi">Retour à la liste</a></div>
            </div>
        </div>
        
    </div>
</div>
</div>
<?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>
</div>
  <?php print render($page['content']['metatags']); ?>




