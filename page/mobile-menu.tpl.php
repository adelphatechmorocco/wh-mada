<div class="responsive-main-menu">
    <div id="o-wrapper" class="o-wrapper responsive-main-menu">
        <button type="button" data-toggle="collapse" id="c-button--slide-right" data-target="#navbar-collapse-2" class="c-button navbar-toggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <nav id="c-menu--slide-right" class="c-menu c-menu--slide-right">
        <button class="c-menu__close">Close Menu &rarr;</button>
        <ul class="c-menu__items parent">
            <li class="c-menu__item"><a href="<?=base_path();?>#webhelp" class="c-menu__link">Qui sommes-nous ?</a></li>
            <li class="c-menu__item"><a href="<?=base_path();?>#avenir" class="c-menu__link">Votre avenir avec nous</a></li>
            <li class="c-menu__item"><a href="<?=base_path();?>#debut-famille" class="c-menu__link">Votre nouvelle famille</a></li>
            <li class="c-menu__item"><a href="<?=base_path();?>#aventure" class="c-menu__link">Envie de nous rejoindre ?</a></li>
        </ul>
        <ul class="nav navbar-nav espace-membre">
            <?php if ( $user->uid ) { ?>
                <li><a href="#0" class="logged_in"><span class="icon-member icon"></span> Bienvenue <?php $author = user_load($user->uid); print $author->field_prenom[und][0][value]; ?></a></li>
            <?php }else{ ?>
                <li><a href="#"><span class="icon-member icon"></span> Espace membre</a></li>
            <?php } ?>
        </ul>
    </nav>
    <div id="c-mask" class="c-mask"></div>
</div>