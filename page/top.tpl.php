<div class="content-top-slide">
    <div class="logo-top wow fadeInLeft">
        <a href="<?=base_path();?>"><img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/logo.png" alt="" class="logo"></a>
        <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/effet-logo.png" alt="" class="fond-logo wow fadeInRight">
    </div>
    <nav class="navbar wow fadeInRight">
        <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-2" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-2">
            <ul class="nav navbar-nav main-menu">
                <li class="active"><a href="<?=base_path();?>#webhelp">Qui sommes-nous ?<?php print theme('header.tpl.php', $args); ?></a></li>
                <li>
                    <a href="<?=base_path();?>#avenir">Votre avenir avec nous</a>
                    <div class="pages">
                        <a href="<?=base_path();?><?php print drupal_get_path_alias('node/52859'); ?>" class="link-formation">Formation</a>
                        <a href="<?=base_path();?><?php print drupal_get_path_alias('node/52874'); ?>" class="link-metiers">Nos Métiers</a>
                        <a href="<?=base_path();?><?php print drupal_get_path_alias('node/52867'); ?>" class="link-evolution">Évolution</a>
                    </div>
                </li>
                <li>
                    <a href="<?=base_path();?>#debut-famille">Votre nouvelle famille</a>
                    <div class="pages">
                        <a href="<?=base_path();?><?php print drupal_get_path_alias('node/52861'); ?>" class="link-bien">Bien-être</a>
                        <a href="<?=base_path();?><?php print drupal_get_path_alias('node/52866'); ?>" class="link-respect">RSE</a>
                        <a href="<?=base_path();?><?php print drupal_get_path_alias('node/52860'); ?>" class="link-avantage">Avantages</a>

                        <a href="<?=base_path();?><?php print drupal_get_path_alias('node/52876'); ?>" class="link-fun">Fun</a>
                    </div>
                </li>
                <li><a href="<?=base_path();?>#aventure">Envie de nous rejoindre ?</a></li>
            </ul>
            <ul class="nav navbar-nav espace-membre">
                <?php if ( $user->uid ) { ?>
                    <li>
                        <a href="#0" class="logged_in">
                            <span class="icon-member icon"></span>
                            Bienvenue
                            <?php
                            $author = user_load($user->uid);
                            print $author->field_prenom[und][0][value];
                            ?>
                        </a>

                        <div class="pages connexion">
                            <a href="<?=base_path();?>cv-webhelp" class="link">Mon CV</a>
                            <a href="<?=base_path();?>user/<?=$user->uid?>/edit" class="link">Mon profil</a>
                            <a href="<?=base_path();?>user/logout" class="link">Déconnexion</a>
                        </div>
                    </li>
                <?php }else{ ?>
                    <li><a href="#"><span class="icon-member icon"></span> Espace membre</a></li>
                <?php } ?>
            </ul>
        </div>
    </nav>
    <?php if(!empty($titre)){ print $titre; } ?>
</div>
