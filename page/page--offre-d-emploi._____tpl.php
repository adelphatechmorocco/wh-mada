<?php 
	$term = taxonomy_term_load($node->field_categorie_actualite[LANGUAGE_NONE][0][tid]);
	$name = $term->name;	
?>
<header class="slide-pages">
    <div class="top-slide top-slide-offres">
        <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
        <div class="titre">
            <h1 class="text-center">Offre d'emploi</h1>
            <h2 class="text-center" style="color: #fff; font-size: 45px"><?=$name?></h2>
        </div>
    </div>
    <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>
</header>

<div class="content-page">
	<div class=" top-page-content">
	     <?php 
	         print $messages; 
	     ?>
		<div id="node-body">
			 <div class="details-offre">
                <div class="content-details-offres">
                    <h2><?=$node->title?></h2>
                    <div class="top-details-offres">
	                    <?php 
							$term = taxonomy_term_load($node->field_ville[LANGUAGE_NONE][0][tid]);
							$ville = $term->name;	
							$term2 = taxonomy_term_load($node->field_metier[LANGUAGE_NONE][0][tid]);
							$metier = $term2->name;
							if($node->field_contrat[LANGUAGE_NONE][0][value]==0)
								$contrat="ANAPEC";
							elseif($node->field_contrat[LANGUAGE_NONE][0][value]==1)
								$contrat="CDD";
							else
								$contrat="CDI";
						?>
                        <p><span>Ville :</span> <?=$ville?> </p>
                        <p><span>Métier :</span> <?=$metier?></p>
                        <p><span>Contrat :</span> <?=$contrat?></p>
                        <div class="link-postuler">
                            <a href="#0">Postuler</a>
                        </div>
                    </div>
                    <?php  print render($node->body[LANGUAGE_NONE][0][value]); ?>
                </div>
            </div>
			 
		</div>
	</div>
</div>
 


