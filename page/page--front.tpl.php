<!--*********************************************************Slider**********************************************************-->
    <div style="width: 1px; color: red; margin: auto"></div>
    <header class="slider-home">
        <?php // region slider
            echo views_embed_view('actualites', 'block_slider');
        ?>

        <div class="top-slide">
            <?php include './'. path_to_theme() .'/templates/page/top.tpl.php'; ?>
        </div>
        <div class="bottom-nav">

            <?php
            $view = views_get_view('offre_emploi');
            $display_id = 'page';
            $view->set_display($display_id);
            $view->init_handlers();
            $form_state = array(
            'view' => $view,
            'display' => $view->display_handler->display,
            'exposed_form_plugin' => $view->display_handler->get_plugin('exposed_form'),
            'method' => 'get',
            'rerender' => TRUE,
            'no_redirect' => TRUE,
            );
            $form = drupal_build_form('views_exposed_form', $form_state);
            print drupal_render($form);
            ?>

            <ul>
                <li><a href="http://www.webhelp.com" target="_blank">Site institutionnel | www.webhelp.com/fr</a></li>
            </ul>
        </div>
    </header>
    <div id="content">
        <?php include './'. path_to_theme() .'/templates/page/mobile-menu.tpl.php'; ?>

<div style="float:left; clear:both; width:100%"><?php print $messages; ?></div>
<?php print render($page['centre']); ?>
        
        <!--*********************************************************Intégration**********************************************************-->
        <div class="integration get-board integration-france" id="integration">
            <div class="container">
                <div class="content-integration">
                    <h2 class="text-center wow zoomIn">Intégration</h2>
                    <p class="integration-text wow zoomIn">C’est parce que les premiers jours dans une entreprise sont un moment crucial<br> que nous avons mis en place une intégration efficace et chaleureuse.</p>

                    <div class="item-get-board">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding text-center zoomIn animated wow">
                            <div class="img-board">
                                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/img-reception.png" alt="">
                            </div>
                            <h3>Accueil</h3>
                            <p class="text-center">Le candidat est <br/>accueilli le jour de <br/>son arrivée.</p>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding text-center wow fadeInUp animated">
                            <div class="img-board">
                                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/signature-img.png" alt="">
                            </div>
                            <h3>Signature <br/>du  contrat</h3>
                            <p class="text-center">Un exemplaire du <br/>contrat est remis au <br/>candidat pour <br/>signature.</p>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding text-center wow zoomIn animated">
                            <div class="img-board">
                                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/boarding-img.png" alt="">
                            </div>
                            <h3>Formation</h3>
                            <p class="text-center">Un parcours <br/>d'intégration/formation <br/>est organisé.</p>
                        </div>
                    </div>
                    <div class="bottom-get-board">
                        <h3 class="text-center">Webhelp, une entreprise où il fait bon vivre &#9786;</h3>
                        <div class="video-get-board">
                            <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/get-on-board-video.png" alt="" class="img-responsive img-video wow zoomIn center-block">
                            <span class="video-link" data-video-id="pXbDEJPujQM" data-video-width="1100" data-video-height="650"><img alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/play-get-board.png" class="zoomIn"></span>
                        </div>
                        <div class="services-webhelp">
                            <div class="services-item">
                                <div class="content-services-item">
                                    <a href="<?php print url('offre-emploi'); ?>">
                                        <div class="imgs cf">
                                            <img class="bottom" alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/offres-bottom-hover.png">
                                            <img class="top" alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/offres.png">
                                        </div>
                                        <span>Nos offres</span>
                                    </a>
                                </div>
                            </div>
                            <div class="services-item">
                                <div class="content-services-item">
                                    <a href="<?php print url('nos-metiers-0'); ?>">
                                        <div class="imgs cf">
                                            <img class="bottom" alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nos-metires-hover.png">
                                            <img class="top" alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/nos-metires.png">
                                        </div>
                                        <span>Nos métiers</span>
                                    </a>
                                </div>
                            </div>
                            <div class="services-item">
                                <div class="content-services-item">
                                    <a href="<?php print url('user/login'); ?>">
                                        <div class="imgs cf">
                                            <img class="bottom" alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/rejoins-nous-hover.png">
                                            <img class="top" alt="" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/rejoins-nous.png">
                                        </div>
                                        <span>Rejoignez nous !</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include './' . path_to_theme() . '/templates/page/block-newsletter.tpl.php'; ?>

    </div>

    
    <ul class="share-btn hidden" id="dvid">
        <li>
            <a href="<?=base_path();?>node/118"> 
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/mail.png" alt="">
            </a>
            <span>Candidature express</span>
        </li>
        <!-- <li>
            <a href="#chat">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/bubble.png" alt="">
            </a>
            <span>Pose tes questions, nous répondons</span>
        </li>-->
        <?php if(!empty($_GET['dev'])){ ?>
	        <li>
	            <a href="#call">
	                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/call.png" alt="">
	            </a>
	            <span class="large">Vous voulez passer votre entretien ?  <br>
	                Nous vous rappelons tout de suite,  <br>
	                ou à <a href="#">l’heure qui vous arrange</a>
	            </span>
	        </li> 
	    <?php } ?>
        <li>
            <a href="#share">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/share.png" alt="">
            </a>
            <ul>
                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#" class="gplus"><i class="fa fa-google-plus"></i></a></li>
            </ul>
        </li>
        <li class="goto">
            <a class="scroll-top" href="#">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/images/prev.png" alt="">
                </a>
        </li>
    </ul>

    

    <!-- share-btn hidden -->
