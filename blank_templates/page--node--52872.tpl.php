<!DOCTYPE html>
<!--[if IE 8]>   <html class="ie8"> <![endif]-->
<!--[if IE 9]>   <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Webhelp Côte d'Ivoire</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,300,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/favicon.ico"> 
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/style-menu-responsive.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/home.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/main.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/custom.css">
    <link rel="stylesheet" href="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/css/mycustom.css">

<body class="map">
    <div id="wrapper" class="wrapper"> 
        <div id="content">
            <div id="map"></div>
            <div class="info-window">test info window</div>
            <div class="logo-bottom">
                <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/logo.png">
            </div> 
            <div class="adresses-webhelp" id="adresses-webhelp">
    
                <div class="ville" id="abidjan">
                    <i class="fa fa-times close-ville" aria-hidden="true"></i>
                        <div class="content-ville"  style="overflow:hidden !important">
                        <a href="<?php print $base_url . $base_path; ?>offre-emploi" class="btn-new">Offres en ligne</a>
                        <h3 class="text-center">Antananarivo</h3>
                        <img src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/images/villes/mada_site.jpg" alt="" />
                        <p>Webhelp Madagascar, <br />Zone Galaxy Andraharo <br />bâtiment Titan II Antananarivo 101

<br />
                           Madagascar

<br />+261 34 11 115 86 
<br />recrutement_mada@webhelp.fr
                           </p>
                     </div>
                </div>            
    
            </div>
        </div>
    </div>

    <div id="wrapper" class="wrapper">
        <div class="map-pays-page">
            <div class="google-map" id="map">
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/bootstrap.min.js"></script>
    <script src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/jquery.easypiechart.min.js"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/main.js?v=3"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDoPCU-YYlKiwPqkSb_PV6ZDBquzApMdwo&v=1"></script>
    <script type="text/javascript" src="<?=base_path().drupal_get_path('theme',$GLOBALS['theme'])?>/js/script.js?v=3"></script>
    <script>
    </script>
</body>

</html>
